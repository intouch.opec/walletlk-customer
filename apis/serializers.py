from datetime import datetime

from django.contrib.auth.models import Group
from django.db.models import Q
from rest_framework import serializers

from apps.models import User, Game, Transaction, TransactionBank, TransactionCredit, Promotion, CustomerLevel, \
    SystemBankAccount, Customer, SystemCreditAccount, CustomerBankAccount, CustomerCreditAccount, Setting
from wallet import utils
from wallet.utils.s3_helper import S3Helper


class GroupSerializer(serializers.ModelSerializer):
    class Meta:
        model = Group
        fields = '__all__'


class ChangePasswordSerializer(serializers.ModelSerializer):
    password = serializers.CharField()
    password_confirmation = serializers.CharField()

    class Meta:
        model = User
        fields = ['password', 'password_confirmation']
        extra_kwargs = {
            'password': {'write_only': True},
            'password_confirmation': {'write_only': True}
        }

    def validate(self, attrs):
        if attrs.get('password') != attrs.get('password_confirmation'):
            raise serializers.ValidationError({'password': 'Password must be confirmed correctly.'})
        return super().validate(attrs)

    def create(self, validated_data):
        pass

    def update(self, instance, validated_data):
        instance.set_password(validated_data['password'])
        instance.save()
        return instance


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = '__all__'
        extra_kwargs = {'password': {'write_only': True}}

    def create(self, validated_data):
        user = super().create(validated_data)
        user.set_password(validated_data['password'])
        user.save()
        return user

    def update(self, instance, validated_data):
        validated_data.pop('username', None)
        return super().update(instance, validated_data)


class CustomerSerializer(serializers.ModelSerializer):
    username = serializers.CharField(max_length=255)
    password = serializers.CharField(max_length=255, write_only=True)
    email = serializers.CharField(max_length=255)
    first_name = serializers.CharField(max_length=255)
    last_name = serializers.CharField(max_length=255)
    customer_level = serializers.DictField()
    bank_accounts = serializers.ListField(child=serializers.DictField())

    class Meta:
        model = Customer
        fields = '__all__'
        extra_kwargs = {
            'password': {'write_only': True},
            'user': {'read_only': True}
        }

    def validate(self, attrs):
        validate_data = super().validate(attrs)
        if self.instance is None:
            bank_no_list = [_['bank_no'] for _ in validate_data['bank_accounts']]
            customer_instance = Customer.objects.filter(
                Q(line_id=validate_data['line_id']) |
                Q(telephone=validate_data['telephone']) |
                Q(user__email=validate_data['email']) |
                Q(user__username=validate_data['username']) |
                Q(user__first_name=validate_data['first_name'], user__last_name=validate_data['last_name']) |
                Q(customerbankaccount__bank_no__in=bank_no_list)
            )
            if customer_instance.exists():
                raise serializers.ValidationError({'user_exists': 'User already exists.'})
        return validate_data

    def create(self, validated_data):
        if 'customer_level' in validated_data:
            validated_data['customer_level_id'] = validated_data['customer_level']['id']
            del validated_data['customer_level']

        bank_accounts = []
        if 'bank_accounts' in validated_data:
            bank_accounts = validated_data['bank_accounts']
            del validated_data['bank_accounts']

        user_data = {
            'email': validated_data['email'],
            'username': validated_data['username'],
            'first_name': validated_data['first_name'],
            'last_name': validated_data['last_name']
        }
        user_password = validated_data['password']

        del validated_data['email']
        del validated_data['username']
        del validated_data['first_name']
        del validated_data['last_name']
        del validated_data['password']

        user_instance = User.objects.create(**user_data)
        user_instance.set_password(user_password)
        user_instance.save()

        validated_data['user'] = user_instance
        customer_instance = super(CustomerSerializer, self).create(validated_data)
        for bank_account in bank_accounts:
            bank_account_bank_no = bank_account['bank_no']
            bank_account_bank_id = bank_account['bank']['id']
            customer_instance.customerbankaccount_set.create(bank_no=bank_account_bank_no, bank_id=bank_account_bank_id)
        return customer_instance

    def update(self, instance, validated_data):
        if 'customer_level' in validated_data:
            validated_data['customer_level_id'] = validated_data['customer_level']['id']
            del validated_data['customer_level']

        bank_accounts = []
        if 'bank_accounts' in validated_data:
            bank_accounts = validated_data['bank_accounts']
            del validated_data['bank_accounts']

        user = User.objects.filter(pk=instance.user_id).first()
        if user:
            if 'password' in validated_data:
                user.set_password(validated_data['password'])
                del validated_data['password']
            user.email = validated_data['email']
            user.first_name = validated_data['first_name']
            user.last_name = validated_data['last_name']
            user.save()

        del validated_data['email']
        del validated_data['first_name']
        del validated_data['last_name']

        instance = super(CustomerSerializer, self).update(instance, validated_data)

        instance.customerbankaccount_set.update(is_active=False)
        for bank_account in bank_accounts:
            bank_account_id = bank_account.get('id')
            bank_account_bank_no = bank_account['bank_no']
            bank_account_bank_id = bank_account['bank']['id']
            if bank_account_id:
                bank_account_instance = instance.customerbankaccount_set.filter(pk=bank_account_id).first()
            else:
                bank_account_instance = instance.customerbankaccount_set.filter(bank_no=bank_account_bank_no).first()

            if bank_account_instance:
                bank_account_instance.bank_no = bank_account_bank_no
                bank_account_instance.bank_id = bank_account_bank_id
                bank_account_instance.is_active = True
                bank_account_instance.save()
            else:
                instance.customerbankaccount_set.create(bank_no=bank_account_bank_no, bank_id=bank_account_bank_id)
        return instance


class PromotionSerializer(serializers.ModelSerializer):
    image_upload = serializers.FileField(required=False, allow_empty_file=True)

    class Meta:
        model = Promotion
        fields = '__all__'

    def create(self, validated_data):
        image_upload = None
        if 'image_upload' in validated_data:
            image_upload = validated_data['image_upload']
            del validated_data['image_upload']

        instance = super().create(validated_data)

        if image_upload:
            s3_helper = S3Helper.get_instance()
            filename = s3_helper.upload_file_to_s3(image_upload)
            instance.image = filename
            instance.save()

        return instance

    def update(self, instance, validated_data):
        image_upload = None
        if 'image_upload' in validated_data:
            image_upload = validated_data['image_upload']
            del validated_data['image_upload']
        instance = super().update(instance, validated_data)

        if image_upload:
            s3_helper = S3Helper.get_instance()
            filename = s3_helper.upload_file_to_s3(image_upload)
            instance.image = filename
            instance.save()

        return instance


class GameSerializer(serializers.ModelSerializer):
    class Meta:
        model = Game
        fields = '__all__'


class CustomerLevelSerializer(serializers.ModelSerializer):
    class Meta:
        model = CustomerLevel
        fields = '__all__'
        extra_kwargs = {'is_default': {'read_only': True}}


class CustomerBankAccountSerializer(serializers.ModelSerializer):
    bank = serializers.DictField()
    customer = serializers.DictField()

    class Meta:
        model = CustomerBankAccount
        fields = '__all__'

    def create(self, validated_data):
        if 'bank' in validated_data:
            validated_data['bank_id'] = validated_data['bank']['id']
            del validated_data['bank']

        if 'customer' in validated_data:
            validated_data['customer_id'] = validated_data['customer']['id']
            del validated_data['customer']
        return super().create(validated_data)

    def update(self, instance, validated_data):
        if 'bank' in validated_data:
            validated_data['bank_id'] = validated_data['bank']['id']
            del validated_data['bank']

        if 'customer' in validated_data:
            validated_data['customer_id'] = validated_data['customer']['id']
            del validated_data['customer']
        return super().update(instance, validated_data)


class CustomerCreditAccountSerializer(serializers.ModelSerializer):
    game = serializers.DictField()
    customer = serializers.DictField()
    promotion = serializers.DictField(required=False, allow_null=True)
    system_credit_account = serializers.DictField(required=False, allow_null=True)
    system_credit_account_id = serializers.IntegerField(required=False, allow_null=True)

    class Meta:
        model = CustomerCreditAccount
        fields = '__all__'

    def create(self, validated_data):
        if 'game' in validated_data:
            validated_data['game_id'] = validated_data['game']['id']
            del validated_data['game']

        if 'customer' in validated_data:
            validated_data['customer_id'] = validated_data['customer']['id']
            del validated_data['customer']

        if 'promotion' in validated_data:
            validated_data['promotion_id'] = validated_data['promotion']['id']
            del validated_data['promotion']

        if 'system_credit_account' in validated_data:
            validated_data['system_credit_account_id'] = validated_data['system_credit_account']['id']
            del validated_data['system_credit_account']

        return super().create(validated_data)

    def update(self, instance, validated_data):
        if 'game' in validated_data:
            validated_data['game_id'] = validated_data['game']['id']
            del validated_data['game']

        if 'customer' in validated_data:
            validated_data['customer_id'] = validated_data['customer']['id']
            del validated_data['customer']

        if 'promotion' in validated_data:
            validated_data['promotion_id'] = validated_data['promotion']['id']
            del validated_data['promotion']

        if 'system_credit_account' in validated_data:
            validated_data['system_credit_account_id'] = validated_data['system_credit_account']['id']
            del validated_data['system_credit_account']

        return super().update(instance, validated_data)


class SystemBankAccountSerializer(serializers.ModelSerializer):
    bank = serializers.DictField()
    customer_levels = serializers.ListField(child=serializers.DictField())

    class Meta:
        model = SystemBankAccount
        fields = ['name', 'balance', 'account_no', 'bank', 'customer_levels', 'is_active']

    def get_fields(self):
        fields = super().get_fields()
        request = self.context.get('request')
        if request and getattr(request, 'method', None) == 'PUT':
            fields['customer_level_ids'].required = False
        return fields

    def create(self, validated_data):
        if 'bank' in validated_data:
            validated_data['bank_id'] = validated_data['bank']['id']
            del validated_data['bank']

        customer_level_ids = []
        if 'customer_levels' in validated_data:
            customer_level_ids = [_['id'] for _ in validated_data['customer_levels']]
            del validated_data['customer_levels']

        instance = super().create(validated_data)
        instance.customer_levels.add(*customer_level_ids)
        return instance

    def update(self, instance, validated_data):
        if 'bank' in validated_data:
            validated_data['bank_id'] = validated_data['bank']['id']
            del validated_data['bank']

        customer_level_ids = []
        if 'customer_levels' in validated_data:
            customer_level_ids = [_['id'] for _ in validated_data['customer_levels']]
            del validated_data['customer_levels']

        instance = super().update(instance, validated_data)
        instance.customer_levels.set(customer_level_ids, clear=True)
        return instance


class SystemCreditAccountSerializer(serializers.ModelSerializer):
    game = serializers.DictField()

    class Meta:
        model = SystemCreditAccount
        fields = ['name', 'balance', 'game', 'is_active']

    def create(self, validated_data):
        if 'game' in validated_data:
            validated_data['game_id'] = validated_data['game']['id']
            del validated_data['game']
        return super().create(validated_data)

    def update(self, instance, validated_data):
        if 'game' in validated_data:
            validated_data['game_id'] = validated_data['game']['id']
            del validated_data['game']
        return super().update(instance, validated_data)


class TransactionDepositSerializer(serializers.ModelSerializer):
    customer_id = serializers.IntegerField(required=False, allow_null=True)
    customer_bank_account_id = serializers.IntegerField(required=False, allow_null=True)
    customer_credit_account_id = serializers.IntegerField(required=False, allow_null=True)
    system_bank_account_id = serializers.IntegerField()
    slip_upload = serializers.FileField(required=False, allow_empty_file=True)
    transaction_at = serializers.CharField(required=False, allow_null=True)

    class Meta:
        model = Transaction
        fields = ['amount', 'note', 'customer_id', 'customer_bank_account_id', 'customer_credit_account_id',
                  'system_bank_account_id', 'slip_upload', 'transaction_at']

    def create(self, validated_data):
        transaction_at = None

        if 'transaction_at' in validated_data:
            transaction_at = datetime.strptime(validated_data['transaction_at'], '%Y-%m-%d %H:%M:%S')
            del validated_data['transaction_at']

        system_bank_account_id = None
        if 'system_bank_account_id' in validated_data:
            system_bank_account_id = validated_data['system_bank_account_id']
            del validated_data['system_bank_account_id']

        slip_upload = None
        if 'slip_upload' in validated_data:
            slip_upload = validated_data['slip_upload']
            del validated_data['slip_upload']

        validated_data['state'] = utils.STATE_DEPOSIT
        instance = super().create(validated_data)
        transaction_bank = TransactionBank.objects.create(transaction=instance, system_bank_account_id=system_bank_account_id, amount=validated_data['amount'],
                                                          transaction_at=transaction_at)
        if slip_upload:
            s3_helper = S3Helper.get_instance()
            filename = s3_helper.upload_file_to_s3(slip_upload)
            transaction_bank.slip = filename
            transaction_bank.save()
        return instance


class TransactionCreditSerializer(serializers.ModelSerializer):
    customer_id = serializers.IntegerField(required=False, allow_null=True)
    customer_bank_account_id = serializers.IntegerField(required=False, allow_null=True)
    customer_credit_account_id = serializers.IntegerField(required=False, allow_null=True)

    class Meta:
        model = Transaction
        fields = ['amount', 'customer_id', 'customer_bank_account_id', 'customer_credit_account_id']

    def create(self, validated_data):
        validated_data['state'] = utils.STATE_WITHDRAW
        instance = super().create(validated_data)
        amount = int(validated_data['amount'])
        convert_to_point = instance.customer_credit_account.game.convert
        credit = int(amount / convert_to_point)
        total_credit = credit
        TransactionCredit.objects.create(transaction=instance, credit=credit, total_credit=total_credit)
        return instance

        
class TransactionBankSerializer(serializers.ModelSerializer):
    transaction = serializers.PrimaryKeyRelatedField(queryset=Transaction.objects.all())
    user = serializers.PrimaryKeyRelatedField(queryset=User.objects.all(), allow_null=True, required=False)
    system_account = serializers.PrimaryKeyRelatedField(queryset=SystemBankAccount.objects.all(), allow_null=True, required=False)

    class Meta:
        model = TransactionBank
        fields = ['id', 'amount', 'user', 'transaction', 'transaction_at', 'status', 'system_account']
