from rest_framework.exceptions import PermissionDenied


def has_permission(request, permission_codename):
    user = request.user
    if user.is_superuser or user.is_staff or user.has_perm(permission_codename):
        return True
    raise PermissionDenied({'detail': 'You do not have permission to perform this action.'})
