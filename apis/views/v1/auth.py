from tokenize import TokenError
from django.utils.translation import ugettext_lazy as _

from rest_framework import status
from rest_framework.response import Response
from rest_framework_simplejwt.exceptions import InvalidToken
from rest_framework_simplejwt.views import TokenObtainPairView


class APIAuthJWT(TokenObtainPairView):
    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        try:
            serializer.is_valid(raise_exception=True)
            user = serializer.user
            if hasattr(user, 'customer'):
                raise TokenError()
            user_data = {
                'fullname': user.get_full_name(),
                'first_name': user.first_name,
                'last_name': user.last_name,
                'username': user.username,
                'email': user.email,
                'is_active': user.is_active,
                'is_superuser': user.is_superuser,
                'is_staff': user.is_staff,
                'permissions': user.get_all_permissions(),
            }
            # device = django_otp.match_token(serializer.user, request.data.get('totp_code'))
            # is_ignore_token_checked = Setting.get_is_ignore_token_checked(request.data.get('ignore_token'))
            # if device is None and is_ignore_token_checked is False:
            #     raise TokenError

        except TokenError as e:
            raise InvalidToken(
                _('username/password mismatch or invalid token. Please make sure you have entered it correctly.'),
                'invalid_credentials'
            )

        response = {
            **user_data,
            **serializer.validated_data
        }
        return Response(response, status=status.HTTP_200_OK)
