from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView

from apis.serializers import CustomerLevelSerializer
from apis.utils import has_permission
from apps.models import CustomerLevel


class CustomerLevelListAPIView(APIView):
    def get_queryset(self):
        queryset = CustomerLevel.objects.order_by('-is_active')
        return queryset

    def get(self, request, *args, **kwargs):
        customer_level_list = []
        customer_levels = self.get_queryset()
        for customer_level in customer_levels:
            context = self.get_customer_level_context(customer_level)
            customer_level_list.append(context)
        return Response(customer_level_list, status=status.HTTP_200_OK)

    def post(self, request, *args, **kwargs):
        has_permission(request, 'apps.can_access_setting')

        customer_level_serializer = CustomerLevelSerializer(data=request.data)
        customer_level_serializer.is_valid(raise_exception=True)
        customer_level = customer_level_serializer.save()
        context = self.get_customer_level_context(customer_level)
        return Response(context, status=status.HTTP_201_CREATED)

    def get_customer_level_context(self, customer_level):
        context = {
            'id': customer_level.pk,
            'name': customer_level.name,
            'is_default': customer_level.is_default,
            'is_active': customer_level.is_active
        }
        return context


class CustomerLevelDetailAPIView(APIView):
    def get_queryset(self):
        pk = self.kwargs.get('pk')
        queryset = CustomerLevel.objects.get(pk=pk)
        return queryset

    def get(self, request, *args, **kwargs):
        has_permission(request, 'apps.can_access_setting')

        self.object = self.get_queryset()
        context = self.get_context_data()
        return Response(context, status=status.HTTP_200_OK)

    def put(self, request, *args, **kwargs):
        has_permission(request, 'apps.can_access_setting')

        customer_level = self.get_queryset()
        customer_level_serializer = CustomerLevelSerializer(customer_level, data=request.data, partial=True)
        customer_level_serializer.is_valid(raise_exception=True)
        self.object = customer_level_serializer.save()
        context = self.get_context_data()
        return Response(context, status=status.HTTP_200_OK)

    def delete(self, request, *args, **kwargs):
        has_permission(request, 'apps.can_access_setting')

        customer_level = self.get_queryset()
        if customer_level.is_default is False:
            customer_level.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

    def get_context_data(self):
        customer_level = self.object
        context = {
            'id': customer_level.pk,
            'name': customer_level.name,
            'is_active': customer_level.is_active
        }
        return context
