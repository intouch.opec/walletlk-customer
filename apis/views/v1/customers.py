from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView

from apis.serializers import CustomerSerializer
from apps.models import Customer, User


class CustomerListView(APIView):
    def get_queryset(self):
        queryset = Customer.objects.all()
        return queryset

    def get(self, request, *args, **kwargs):
        customer_list = []
        customers = self.get_queryset()
        for customer in customers:
            customer_list.append(customer.context_data)

        return Response(customer_list, status=status.HTTP_200_OK)

    def post(self, request, *args, **kwargs):
        customer_serializer = CustomerSerializer(data=request.data)
        customer_serializer.is_valid(raise_exception=True)
        customer = customer_serializer.save()
        return Response(customer.context_data, status=status.HTTP_201_CREATED)


class CustomerDetailView(APIView):
    def get_queryset(self):
        pk = self.kwargs.get('pk')
        queryset = Customer.objects.prefetch_related('customerbankaccount_set', 'customercreditaccount_set').get(pk=pk)
        return queryset

    def get(self, request, *args, **kwargs):
        self.object = self.get_queryset()
        context = self.get_context_data()
        return Response(context, status=status.HTTP_200_OK)

    def put(self, request, *args, **kwargs):
        customer = self.get_queryset()
        customer_serializer = CustomerSerializer(customer, data=request.data, partial=True)
        customer_serializer.is_valid(raise_exception=True)
        self.object = customer_serializer.save()
        context = self.get_context_data()
        return Response(context, status=status.HTTP_200_OK)

    def delete(self, request, *args, **kwargs):
        customer = self.get_queryset()
        customer.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

    def get_context_data(self):
        customer = self.object
        history_list = []
        limit_history = self.kwargs.get('limit_history')
        transactions = customer.transaction_set.order_by('-id')
        if limit_history:
            transactions = transactions[:limit_history]

        for transaction in transactions:
            history_list.append({
                'state': transaction.state,
                'amount': transaction.amount,
                'status': transaction.status,
                'created_at': transaction.created_at
            })

        customer_bank_accounts = []
        for customer_bank_account in customer.customerbankaccount_set.select_related('bank').filter(is_active=True):
            customer_bank_accounts.append(customer_bank_account.context_data)

        customer_credit_accounts = []
        for customer_credit_account in customer.customercreditaccount_set.select_related('game'):
            customer_credit_accounts.append(customer_credit_account.context_data)

        context = customer.context_data
        context['bank_accounts'] = customer_bank_accounts
        context['credit_accounts'] = customer_credit_accounts
        context['transaction_history'] = history_list
        return context
