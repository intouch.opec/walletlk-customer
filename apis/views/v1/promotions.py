from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView

from apis.serializers import PromotionSerializer
from apis.utils import has_permission
from apps.models import Promotion
from wallet.utils.s3_helper import S3Helper


class PromotionListAPIView(APIView):
    def get_queryset(self):
        queryset = Promotion.objects.all()
        return queryset

    def get(self, request, *args, **kwargs):
        has_permission(request, 'apps.can_access_promotion')

        promotion_list = []
        promotions = self.get_queryset()
        for promotion in promotions:
            promotion_data = self.get_promotion_context(promotion)
            promotion_list.append(promotion_data)
        return Response(promotion_list, status=status.HTTP_200_OK)

    def post(self, request, *args, **kwargs):
        has_permission(request, 'apps.can_access_promotion')

        promotion_serializer = PromotionSerializer(data=request.data)
        promotion_serializer.is_valid(raise_exception=True)
        promotion = promotion_serializer.save()
        context = self.get_promotion_context(promotion)
        return Response(context, status=status.HTTP_201_CREATED)

    def get_promotion_context(self, promotion):
        context = promotion.context_data
        return context


class PromotionDetailAPIView(APIView):
    def get_queryset(self):
        pk = self.kwargs.get('pk')
        queryset = Promotion.objects.get(pk=pk)
        return queryset

    def get(self, request, *args, **kwargs):
        has_permission(request, 'apps.can_access_promotion')

        self.object = self.get_queryset()
        context = self.object.context_data
        return Response(context, status=status.HTTP_200_OK)

    def put(self, request, *args, **kwargs):
        has_permission(request, 'apps.can_access_promotion')

        promotion = self.get_queryset()
        promotion_serializer = PromotionSerializer(promotion, data=request.data, partial=True)
        promotion_serializer.is_valid(raise_exception=True)
        self.object = promotion_serializer.save()
        context = self.object.context_data
        return Response(context, status=status.HTTP_200_OK)

    def delete(self, request, *args, **kwargs):
        has_permission(request, 'apps.can_access_promotion')

        promotion = self.get_queryset()
        promotion.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


class PromotionDeleteImageAPIView(APIView):

    def get_queryset(self):
        pk = self.kwargs.get('pk')
        queryset = Promotion.objects.get(pk=pk)
        return queryset

    def delete(self, request, *args, **kwargs):
        has_permission(request, 'apps.can_access_promotion')
        promotion = self.get_queryset()
        if promotion.image:
            s3_helper = S3Helper.get_instance()
            s3_helper.delete_file_from_s3(filename=promotion.image)
            promotion.image = None
            promotion.save()

        return Response(promotion.context_data, status=status.HTTP_200_OK)
