from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView

from apis.serializers import SystemCreditAccountSerializer
from apis.utils import has_permission
from apps.models import SystemCreditAccount, LogSystemCreditAccount
from wallet import utils


class SystemAccountListAPIView(APIView):
    def get_queryset(self):
        queryset = SystemCreditAccount.objects.select_related('game')
        exclude_inactive_status = self.request.query_params.get('is_active', False)
        exclude_inactive_game = self.request.query_params.get('game', False)
        if exclude_inactive_status:
            queryset = queryset.filter(is_active=True)

        if exclude_inactive_game:
            queryset = queryset.filter(game_id=exclude_inactive_game)

        return queryset.order_by('-is_active')

    def get(self, request, *args, **kwargs):
        system_credit_account_list = []
        system_credit_accounts = self.get_queryset()
        for system_credit_account in system_credit_accounts:
            system_credit_account_list.append(system_credit_account.context_data)
        return Response(system_credit_account_list, status=status.HTTP_200_OK)

    def post(self, request, *args, **kwargs):
        has_permission(request, 'apps.can_access_system_account')

        system_credit_account_serializer = SystemCreditAccountSerializer(data=request.data)
        system_credit_account_serializer.is_valid(raise_exception=True)
        system_credit_account = system_credit_account_serializer.save()

        LogSystemCreditAccount.objects.create(
            old_balance=0,
            new_balance=system_credit_account.balance,
            user=request.user,
            system_credit_account=system_credit_account,
            action=utils.ACTION_INITIAL_BALANCE
        )
        return Response(system_credit_account.context_data, status=status.HTTP_201_CREATED)


class SystemAccountDetailAPIView(APIView):
    def get_queryset(self):
        pk = self.kwargs.get('pk')
        queryset = SystemCreditAccount.objects.select_related('game').get(pk=pk)
        return queryset

    def get(self, request, *args, **kwargs):
        has_permission(request, 'apps.can_access_system_account')

        self.object = self.get_queryset()
        context = self.get_context_data()
        return Response(context, status=status.HTTP_200_OK)

    def put(self, request, *args, **kwargs):
        has_permission(request, 'apps.can_access_system_account')

        system_credit_account = self.get_queryset()
        system_credit_account_old_balance = system_credit_account.balance

        system_credit_account_serializer = SystemCreditAccountSerializer(system_credit_account, data=request.data, partial=True)
        system_credit_account_serializer.is_valid(raise_exception=True)
        self.object = system_credit_account_serializer.save()

        LogSystemCreditAccount.objects.create(
            old_balance=system_credit_account_old_balance,
            new_balance=self.object.balance,
            user=request.user,
            system_credit_account=self.object,
            action=utils.ACTION_UPDATE_BALANCE
        )
        context = self.get_context_data()
        return Response(context, status=status.HTTP_200_OK)

    def delete(self, request, *args, **kwargs):
        has_permission(request, 'apps.can_access_system_account')

        system_account = self.get_queryset()
        system_account.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

    def get_context_data(self):
        system_credit_account = self.object
        context = system_credit_account.context_data
        return context
