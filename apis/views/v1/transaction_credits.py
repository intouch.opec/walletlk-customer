from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView

from apis.utils import has_permission
from apps.models import TransactionCredit, TransactionBank, SystemCreditAccount, LogSystemCreditAccount
from wallet import utils
from wallet.utils.pusher_services import PusherService


class TransactionCreditListAPIView(APIView):
    def check_has_permission(self):
        has_permission(self.request, 'apps.can_access_transaction_credit')

    def get_queryset(self):
        queryset = TransactionCredit.objects.filter(status=utils.STATUS_PENDING).select_related(
            'user', 'transaction', 'transaction__customer', 'transaction__customer_credit_account',
            'transaction__customer_credit_account__game'
        )
        return queryset

    def get(self, request, *args, **kwargs):
        self.check_has_permission()

        transaction_credit_list = []
        transaction_credits = self.get_queryset()
        for transaction_credit in transaction_credits:
            transaction_credit_list.append(transaction_credit.context_data)
        return Response(transaction_credit_list)


class TransactionCreditDetailAPIView(APIView):
    def check_has_permission(self):
        has_permission(self.request, 'apps.can_access_transaction_credit')

    def get_queryset(self):
        pk = self.kwargs.get('pk')
        queryset = TransactionCredit.objects.filter(status=utils.STATUS_PENDING).select_related(
            'user', 'transaction', 'transaction__customer', 'transaction__customer_credit_account',
            'transaction__customer_credit_account__game', 'transaction__customer_credit_account__system_credit_account'
        ).get(pk=pk)
        return queryset

    def get(self, request, *args, **kwargs):
        self.check_has_permission()

        transaction_credit = self.get_queryset()
        history_list = []
        customer = transaction_credit.transaction.customer
        transactions = customer.transaction_set.order_by('-id')[:10]
        for transaction in transactions:
            history_list.append({
                'state': transaction.state,
                'amount': transaction.amount,
                'status': transaction.status,
                'created_at': transaction.created_at
            })

        context = transaction_credit.context_data
        context['transaction_history'] = history_list
        return Response(context, status=status.HTTP_200_OK)

    def post(self, request, *args, **kwargs):
        self.check_has_permission()

        remark = request.data.get('remark')
        is_approved = request.data.get('is_approved', False)
        bonus_credit = request.data.get('bonus_credit', 0)

        transaction_credit = self.get_queryset()
        transaction = transaction_credit.transaction
        if is_approved:
            system_credit_account = transaction.customer_credit_account.system_credit_account
            system_credit_account_old_balance = system_credit_account.balance
            if transaction.state == utils.STATE_DEPOSIT:
                transaction_credit.status = utils.STATUS_APPROVED
                transaction_credit.system_credit_account = system_credit_account
                transaction_credit.bonus_credit = bonus_credit
                transaction_credit.total_credit = transaction_credit.credit + int(bonus_credit)
                transaction_credit.user = request.user
                transaction_credit.save()

                system_credit_account.balance = system_credit_account_old_balance - transaction_credit.total_credit
                system_credit_account.save()

                transaction.status = utils.STATUS_APPROVED
                transaction.save()
                self.emit_message(transaction, emit_event='remove')
            else:
                transaction_credit.status = utils.STATUS_APPROVED
                transaction_credit.system_credit_account = system_credit_account
                transaction_credit.user = request.user
                transaction_credit.save()

                system_credit_account.balance = system_credit_account_old_balance + transaction_credit.total_credit
                system_credit_account.save()

                amount = transaction_credit.total_credit * system_credit_account.game.convert
                transaction_bank = TransactionBank(amount=amount, transaction=transaction)
                transaction_bank.save()
                self.emit_message(transaction)

            LogSystemCreditAccount.objects.create(
                old_balance=system_credit_account_old_balance,
                new_balance=system_credit_account.balance,
                user=request.user,
                transaction=transaction,
                system_credit_account=system_credit_account,
                action=utils.ACTION_UPDATE_BALANCE
            )
            return Response({'message': 'Transaction credit was approved.'}, status=status.HTTP_200_OK)
        else:
            transaction_credit.status = utils.STATUS_REJECTED
            transaction_credit.user = request.user
            transaction_credit.save()

            transaction.status = utils.STATUS_REJECTED
            transaction.remark = remark
            transaction.save()
            self.emit_message(transaction, emit_event='remove')
            return Response({'message': 'Transaction credit was rejected.'}, status=status.HTTP_200_OK)

    def put(self, request, *args, **kwargs):
        self.check_has_permission()

        action = request.data.get('action')
        if action == 'updateProgress':
            transaction_credit = self.get_queryset()
            transaction_credit.user = request.user
            transaction_credit.save()
            self.emit_message(transaction_credit.transaction, emit_event='update')
            return Response(status=status.HTTP_200_OK)

        return Response(status=status.HTTP_405_METHOD_NOT_ALLOWED)

    def emit_message(self, transaction, emit_event='new'):
        pusher_service = PusherService.get_instance()
        emit_channel_bank = 'transaction-bank'
        emit_channel_credit = 'transaction-credit'
        if emit_event == 'new':
            pusher_service.emit(emit_channel_bank, 'new', transaction.transactionbank.context_data)
            pusher_service.emit('system', 'alert', {'message': 'มีรายการการเงินใหม่'})
        elif emit_event == 'update':
            pusher_service.emit(emit_channel_credit, 'update', transaction.transactioncredit.context_data)

        if emit_event in ['new', 'remove']:
            pusher_service.emit(emit_channel_credit, 'remove', {'id': transaction.transactioncredit.id})
