from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView

from apis.serializers import TransactionCreditSerializer, TransactionDepositSerializer
from apis.utils import has_permission
from apps.models import Transaction, TransactionBank
from wallet.utils.pusher_services import PusherService


class TransactionListAPIView(APIView):
    def check_has_permission(self):
        has_permission(self.request, 'apps.can_access_transaction_history')

    def get_queryset(self):
        queryset = Transaction.objects.order_by('-created_at')
        return queryset

    def get(self, request, *args, **kwargs):
        self.check_has_permission()

        transaction_list = []
        transactions = self.get_queryset()
        for transaction in transactions:
            transaction_list.append(transaction.context_data)
        return Response(transaction_list, status=status.HTTP_200_OK)


class TransactionDetailAPIView(APIView):
    def check_has_permission(self):
        has_permission(self.request, 'apps.can_access_transaction_history')

    def get_queryset(self):
        pk = self.kwargs.get('pk')
        queryset = Transaction.objects.get(pk=pk)
        return queryset

    def get(self, request, *args, **kwargs):
        self.check_has_permission()

        self.object = self.get_queryset()
        context = self.get_context_data()
        return Response(context, status=status.HTTP_200_OK)

    def get_context_data(self):
        transaction = self.object
        transaction_bank = None
        transaction_credit = None
        if hasattr(transaction, 'transactionbank'):
            transaction_bank = transaction.transactionbank.context_data
        if hasattr(transaction, 'transactioncredit'):
            transaction_credit = transaction.transactioncredit.context_data

        return {
            **transaction.context_data,
            'transaction_bank': transaction_bank,
            'transaction_credit': transaction_credit
        }


class TransactionDepositAPIView(APIView):
    def post(self, request, *args, **kwargs):
        transaction_serializer = TransactionDepositSerializer(data=request.data)
        transaction_serializer.is_valid(raise_exception=True)
        transaction = transaction_serializer.save()
        transaction.user = request.user
        transaction.save()
        self.emit_message(transaction)
        return Response(status=status.HTTP_201_CREATED)

    def emit_message(self, transaction):
        pusher_service = PusherService.get_instance()
        message = {'message': 'มีรายการการเงินใหม่'}
        pusher_service.emit('transaction-bank', 'new', transaction.transactionbank.context_data)
        pusher_service.emit('system', 'alert', message)


class TransactionWithdrawAPIView(APIView):
    def post(self, request, *args, **kwargs):
        transaction_serializer = TransactionCreditSerializer(data=request.data)
        transaction_serializer.is_valid(raise_exception=True)
        transaction = transaction_serializer.save()
        transaction.user = request.user
        transaction.save()
        self.emit_message(transaction)
        return Response(status=status.HTTP_201_CREATED)

    def emit_message(self, transaction):
        pusher_service = PusherService.get_instance()
        message = {'message': 'มีรายการเครดิตใหม่'}
        pusher_service.emit('transaction-credit', 'new', transaction.transactioncredit.context_data)
        pusher_service.emit('system', 'alert', message)
