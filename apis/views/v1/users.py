from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView

from apis.serializers import UserSerializer, ChangePasswordSerializer
from apis.utils import has_permission
from apps.models import User


class UserListAPIView(APIView):
    def get_queryset(self):
        queryset = User.objects.filter(is_superuser=False, customer__isnull=True).exclude(pk=self.request.user.pk)
        return queryset

    def get(self, request, *args, **kwargs):
        has_permission(request, 'apps.can_access_user')

        user_list = []
        users = self.get_queryset()
        for user in users:
            context = self.get_user_context(user)
            user_list.append(context)
        return Response(user_list, status=status.HTTP_200_OK)

    def post(self, request, *args, **kwargs):
        has_permission(request, 'apps.can_access_user')

        user_serializer = UserSerializer(data=request.data)
        user_serializer.is_valid(raise_exception=True)
        user = user_serializer.save()

        group = request.data.get('group')
        if group:
            user.groups.add(group['id'])

        permissions = [permission['id'] for permission in request.data.get('permissions', [])]
        user.user_permissions.set(permissions)

        context = self.get_user_context(user)
        return Response(context, status=status.HTTP_201_CREATED)

    def get_user_context(self, user):
        group_name = None
        user_group = user.groups.first()
        if user_group:
            group_name = user_group.name
        return {
            'id': user.pk,
            'username': user.username,
            'fullname': user.get_full_name(),
            'is_staff': user.is_staff,
            'is_active': user.is_active,
            'group': group_name
        }


class UserDetailAPIView(APIView):
    def get_queryset(self):
        pk = self.kwargs.get('pk')
        queryset = User.objects.filter(is_superuser=False, customer__isnull=True).exclude(pk=self.request.user.pk).get(pk=pk)
        return queryset

    def get(self, request, *args, **kwargs):
        has_permission(request, 'apps.can_access_user')

        self.object = self.get_queryset()
        context = self.get_context_data()
        return Response(context, status=status.HTTP_200_OK)

    def put(self, request, *args, **kwargs):
        has_permission(request, 'apps.can_access_user')

        user = self.get_queryset()
        user_serializer = UserSerializer(user, data=request.data, partial=True)
        user_serializer.is_valid(raise_exception=True)
        self.object = user_serializer.save()

        group = request.data.get('group')
        user.groups.clear()
        if group:
            user.groups.add(group['id'])

        permissions = [permission['id'] for permission in request.data.get('permissions', [])]
        user.user_permissions.set(permissions, clear=True)

        context = self.get_context_data()
        return Response(context, status=status.HTTP_200_OK)

    def delete(self, request, *args, **kwargs):
        has_permission(request, 'apps.can_access_user')

        user = self.get_queryset()
        user.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

    def get_context_data(self):
        user = self.object
        user_group = user.groups.first()
        user_permissions = user.user_permissions.all()

        group = None
        if user_group:
            group = {
                'id': user_group.id,
                'name': user_group.name
            }

        context = {
            'id': user.pk,
            'username': user.username,
            'first_name': user.first_name,
            'last_name': user.last_name,
            'is_staff': user.is_staff,
            'is_active': user.is_active,
            'group': group,
            'permissions': [{
                'id': user_permission.id,
                'name': user_permission.name,
                'codename': user_permission.codename
            } for user_permission in user_permissions]
        }
        return context


class ProfileAPIView(APIView):
    def get(self, request, *args, **kwargs):
        context = self.get_context_data()
        return Response(context, status=status.HTTP_200_OK)

    def post(self, request, *args, **kwargs):
        user_serializer = UserSerializer(self.request.user, data=request.data, partial=True)
        user_serializer.is_valid(raise_exception=True)
        self.object = user_serializer.save()
        context = self.get_context_data()
        return Response(context, status=status.HTTP_200_OK)

    def get_context_data(self):
        user = self.object
        context = {
            'email': user.email,
            'username': user.username,
            'first_name': user.first_name,
            'last_name': user.last_name,
            'fullname': user.get_full_name()
        }
        return context


class ChangePasswordAPIView(APIView):
    def get_queryset(self):
        pk = self.kwargs.get('pk')
        queryset = User.objects.get(pk=pk)
        return queryset

    def post(self, request, *args, **kwargs):
        user = self.get_queryset()
        user_serializer = ChangePasswordSerializer(user, data=request.data)
        user_serializer.is_valid(raise_exception=True)
        user_serializer.save()
        return Response(status=status.HTTP_200_OK)
