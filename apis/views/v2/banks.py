from rest_framework import status
from rest_framework.permissions import IsAuthenticatedOrReadOnly
from rest_framework.response import Response
from rest_framework.views import APIView

from apps.models import Bank, CustomerCreditAccount


class BankListAPIView(APIView):
    permission_classes = [IsAuthenticatedOrReadOnly]

    def get_queryset(self):
        queryset = Bank.objects.all()
        return queryset

    def get(self, request, *args, **kwargs):
        banks = self.get_queryset()
        bank_list = []
        for bank in banks:
            bank_list.append(bank.context_data)
        return Response(bank_list, status=status.HTTP_200_OK)


