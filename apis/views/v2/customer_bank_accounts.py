from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView

from apps.models import CustomerBankAccount


class CustomerBankAccountListAPIView(APIView):

    def get(self, request, *args, **kwargs):
        customer = request.user.customer
        customer_bank_accounts = customer.customerbankaccount_set.all()
        customer_bank_account_list = []
        for customer_bank_account in customer_bank_accounts:
            customer_bank_account_list.append(customer_bank_account.context_data)
        return Response(customer_bank_account_list, status=status.HTTP_200_OK)
