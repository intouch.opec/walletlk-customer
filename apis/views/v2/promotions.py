from rest_framework import status
from rest_framework.permissions import IsAuthenticatedOrReadOnly
from rest_framework.response import Response
from rest_framework.views import APIView


from apps.models import Promotion


class PromotionListAPIView(APIView):
    permission_classes = [IsAuthenticatedOrReadOnly]

    def get_queryset(self):
        queryset = Promotion.objects.all()
        return queryset

    def get(self, request, *args, **kwargs):
        promotion_list = []
        promotions = self.get_queryset()
        for promotion in promotions:
            promotion_list.append(promotion.context_data)
        return Response(promotion_list, status=status.HTTP_200_OK)
