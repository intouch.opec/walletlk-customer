from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView

from apps.models import SystemBankAccount


class SystemBankAccountListAPIView(APIView):

    def get_queryset(self):
        customer_level = self.request.user.customer.customer_level
        queryset = SystemBankAccount.objects.filter(customer_levels=customer_level)
        return queryset

    def get(self, request, *args, **kwargs):
        system_bank_accounts = self.get_queryset()
        system_bank_account_list = []
        for system_bank_account in system_bank_accounts:
            system_bank_account_list.append(system_bank_account.context_data)
        return Response(system_bank_account_list, status=status.HTTP_200_OK)
