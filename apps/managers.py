from django.contrib.auth.models import UserManager
from safedelete.managers import SafeDeleteManager


class UserSafeDeleteManager(UserManager, SafeDeleteManager):
    pass


class CustomerManager(SafeDeleteManager):
    def get_queryset(self):
        return super().get_queryset().select_related('user', 'customer_level')
