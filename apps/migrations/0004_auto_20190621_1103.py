# Generated by Django 2.2 on 2019-06-21 11:03

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('apps', '0003_transactionbank_transaction_at'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='user',
            options={'permissions': [('can_access_transaction_bank', 'Can Access Transaction Bank'), ('can_access_transaction_credit', 'Can Access Transaction Credit'), ('can_access_transaction_history', 'Can Access Transaction History'), ('can_access_report', 'Can Access Report'), ('can_access_system_account', 'Can Access System Account'), ('can_access_promotion', 'Can Access Promotion'), ('can_access_setting', 'Can Access Setting'), ('can_setting_wallet_lk', 'Can Access Setting Web Wallet_lk')]},
        ),
        migrations.RemoveField(
            model_name='transactioncredit',
            name='system_credit_account',
        ),
        migrations.AddField(
            model_name='customercreditaccount',
            name='system_credit_account',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='apps.SystemCreditAccount'),
        ),
    ]
