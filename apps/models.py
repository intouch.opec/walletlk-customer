from django.contrib.auth.models import AbstractUser
from django.db import models
from django.urls import reverse

from apps import managers
from wallet import utils


class User(utils.BaseModel, AbstractUser):
    REQUIRED_FIELDS = []
    objects = managers.UserSafeDeleteManager()

    class Meta:
        permissions = [
            ('can_access_transaction_bank', 'Can Access Transaction Bank'),
            ('can_access_transaction_credit', 'Can Access Transaction Credit'),
            ('can_access_transaction_history', 'Can Access Transaction History'),
            ('can_access_report', 'Can Access Report'),
            ('can_access_system_account', 'Can Access System Account'),
            ('can_access_promotion', 'Can Access Promotion'),
            ('can_access_setting', 'Can Access Setting'),
            ('can_setting_wallet_lk', 'Can Access Setting Web Wallet_lk'),
        ]

    @property
    def context_data(self):
        return {
            'id': self.pk,
            'email': self.email,
            'username': self.username,
            'first_name': self.first_name,
            'last_name': self.last_name,
            'fullname': self.get_full_name(),
        }


class Bank(utils.BaseModel):
    name = models.CharField(max_length=255)
    code = models.CharField(max_length=10, db_index=True, unique=True)

    def __str__(self):
        return '{} ({})'.format(self.name, self.code)

    @property
    def context_data(self):
        return {
            'id': self.pk,
            'name': self.name,
            'code': self.code
        }


class Game(utils.BaseModel):
    name = models.CharField(max_length=255)
    convert = models.IntegerField()

    def __str__(self):
        return self.name

    @property
    def context_data(self):
        return {
            'id': self.pk,
            'name': self.name,
            'convert': self.convert
        }


class Promotion(utils.BaseModel):
    name = models.CharField(max_length=255)
    detail = models.TextField(blank=True, null=True)
    is_active = models.BooleanField(default=True)
    image = models.CharField(max_length=255, blank=True, null=True)

    def __str__(self):
        return self.name

    @property
    def context_data(self):
        image = None
        if self.image:
            image = reverse('apps:media', kwargs={'filename': self.image})
        return {
            'id': self.pk,
            'name': self.name,
            'detail': self.detail,
            'is_active': self.is_active,
            'image': image
        }


class CustomerLevel(utils.BaseModel):
    name = models.CharField(max_length=255)
    is_default = models.BooleanField(default=False)
    is_active = models.BooleanField(default=True)

    def __str__(self):
        return self.name

    @property
    def context_data(self):
        return {
            'id': self.pk,
            'name': self.name,
            'is_default': self.is_default,
            'is_active': self.is_active
        }


class SystemBankAccount(utils.BaseModel):
    name = models.CharField(max_length=255)
    balance = models.CharField(max_length=20)
    account_no = models.CharField(max_length=255)
    bank = models.ForeignKey(Bank, on_delete=models.CASCADE)
    customer_levels = models.ManyToManyField(CustomerLevel)
    is_active = models.BooleanField(default=True)

    def __str__(self):
        return '{} ({})'.format(self.name, self.account_no)

    @property
    def context_data(self):
        return {
            'id': self.pk,
            'name': self.name,
            'account_no': self.account_no,
            'balance': float(self.balance),
            'bank': self.bank.context_data,
            'is_active': self.is_active
        }


class SystemCreditAccount(utils.BaseModel):
    name = models.CharField(max_length=255)
    balance = models.IntegerField()
    game = models.ForeignKey(Game, on_delete=models.CASCADE)
    is_active = models.BooleanField(default=True)

    def __str__(self):
        return '[{}] - {}'.format(self.game, self.name)

    @property
    def context_data(self):
        return {
            'id': self.pk,
            'name': self.name,
            'balance': self.balance,
            'is_active': self.is_active,
            'game': self.game.context_data
        }


class Customer(utils.BaseModel):
    telephone = models.CharField(max_length=255, blank=True, null=True)
    line_id = models.CharField(max_length=255, blank=True, null=True)
    customer_level = models.ForeignKey(CustomerLevel, on_delete=models.SET_NULL, blank=True, null=True)
    user = models.OneToOneField(User, on_delete=models.CASCADE)

    objects = managers.CustomerManager()

    def __str__(self):
        return self.user.get_full_name()

    @property
    def context_data(self):
        return {
            **self.user.context_data,
            'user_id': self.user.id,
            'id': self.pk,
            'telephone': self.telephone,
            'line_id': self.line_id,
            'customer_level': self.customer_level.context_data
        }


class CustomerBankAccount(utils.BaseModel):
    bank_no = models.CharField(max_length=255, blank=True, null=True)
    bank = models.ForeignKey(Bank, on_delete=models.CASCADE)
    customer = models.ForeignKey(Customer, on_delete=models.CASCADE)
    is_active = models.BooleanField(default=True)

    def __str__(self):
        return '[{}] - {}'.format(self.bank, self.bank_no)

    @property
    def context_data(self):
        return {
            'id': self.pk,
            'bank_no': self.bank_no,
            'is_active': self.is_active,
            'bank': self.bank.context_data,
            'customer_id': self.customer_id
        }


class CustomerCreditAccount(utils.BaseModel):
    username = models.CharField(max_length=255, blank=True, null=True)
    password = models.CharField(max_length=255, blank=True, null=True)
    game = models.ForeignKey(Game, on_delete=models.CASCADE)
    customer = models.ForeignKey(Customer, on_delete=models.CASCADE)
    status = models.IntegerField(choices=utils.CHOICE_STATUS, default=utils.STATUS_PENDING)
    system_credit_account = models.ForeignKey(SystemCreditAccount, on_delete=models.CASCADE, blank=True, null=True)
    promotion = models.ForeignKey(Promotion, on_delete=models.CASCADE, blank=True, null=True)

    def __str__(self):
        return '[{}] - {}'.format(self.game, self.username)

    @property
    def context_data(self):
        system_credit_account = self.system_credit_account
        if system_credit_account:
            system_credit_account = self.system_credit_account.context_data

        promotion = self.promotion
        if promotion:
            promotion = self.promotion.context_data

        return {
            'id': self.pk,
            'username': self.username,
            'password': self.password,
            'status': self.status,
            'game': self.game.context_data,
            'customer': self.customer.context_data,
            'system_credit_account': system_credit_account,
            'promotion': promotion
        }


class Transaction(utils.BaseModel):
    amount = models.CharField(max_length=20)
    state = models.IntegerField(choices=utils.CHOICE_STATE)
    status = models.IntegerField(choices=utils.CHOICE_STATUS, default=utils.STATUS_PENDING)
    user = models.ForeignKey(User, on_delete=models.CASCADE, blank=True, null=True)
    customer = models.ForeignKey(Customer, on_delete=models.CASCADE, blank=True, null=True)
    customer_bank_account = models.ForeignKey(CustomerBankAccount, on_delete=models.CASCADE, blank=True, null=True)
    customer_credit_account = models.ForeignKey(CustomerCreditAccount, on_delete=models.CASCADE, blank=True, null=True)
    note = models.CharField(max_length=255, blank=True, null=True)
    remark = models.CharField(max_length=255, blank=True, null=True)

    def __str__(self):
        return '[{}] - {}'.format(self.get_state_display(), self.pk)

    @property
    def context_data(self):
        user = self.user
        if user:
            user = user.context_data
        customer = self.customer
        if customer:
            customer = customer.context_data
        customer_bank_account = self.customer_bank_account
        if customer_bank_account:
            customer_bank_account = customer_bank_account.context_data
        customer_credit_account = self.customer_credit_account
        if customer_credit_account:
            customer_credit_account = customer_credit_account.context_data
        return {
            'id': self.pk,
            'amount': self.amount,
            'note': self.note,
            'remark': self.remark,
            'customer': customer,
            'customer_bank_account': customer_bank_account,
            'customer_credit_account': customer_credit_account,
            'user': user,
            'state': self.state,
            'status': self.status,
            'created_at': self.created_at,
            'updated_at': self.created_at
        }


class TransactionBank(utils.BaseModel):
    amount = models.CharField(max_length=20)
    status = models.IntegerField(choices=utils.CHOICE_STATUS, default=utils.STATUS_PENDING)
    transaction = models.OneToOneField(Transaction, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE, blank=True, null=True)
    system_bank_account = models.ForeignKey(SystemBankAccount, on_delete=models.CASCADE, blank=True, null=True)
    slip = models.CharField(max_length=255, blank=True, null=True)
    transaction_at = models.DateTimeField(blank=True, null=True)

    def __str__(self):
        return '{}'.format(self.transaction)

    @property
    def context_data(self):
        transaction = self.transaction
        customer = transaction.customer
        if customer:
            customer = customer.context_data
        user = self.user
        if user:
            user = user.context_data

        slip = None
        if self.slip:
            slip = reverse('apps:media', kwargs={'filename': self.slip})

        transaction_at = self.transaction_at
        if transaction_at:
            transaction_at = str(transaction_at)

        return {
            'id': self.pk,
            'customer': customer,
            'bank_account': self.get_bank_account_detail(transaction),
            'transaction_at': transaction_at,
            'created_at': str(self.created_at),
            'amount': self.amount,
            'status': self.status,
            'state': transaction.state,
            'note': transaction.note,
            'user': user,
            'slip': slip
        }

    def get_bank_account_detail(self, transaction):
        if transaction.state == utils.STATE_DEPOSIT:
            return self.system_bank_account.context_data
        else:
            return transaction.customer_bank_account.context_data


class TransactionCredit(utils.BaseModel):
    credit = models.IntegerField()
    bonus_credit = models.IntegerField(default=0)
    total_credit = models.IntegerField()
    status = models.IntegerField(choices=utils.CHOICE_STATUS, default=utils.STATUS_PENDING)
    transaction = models.OneToOneField(Transaction, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE, blank=True, null=True)

    def __str__(self):
        return '{}'.format(self.transaction)

    @property
    def context_data(self):
        transaction = self.transaction
        customer = transaction.customer
        customer_credit_account = transaction.customer_credit_account
        user = self.user
        if user:
            user = user.context_data

        return {
            'id': self.id,
            'customer':  customer.context_data,
            'customer_credit_account': customer_credit_account.context_data,
            'credit': self.credit,
            'bonus_credit': self.bonus_credit,
            'total_credit': self.total_credit,
            'status': self.status,
            'state': transaction.state,
            'game': customer_credit_account.game.context_data,
            'user': user,
            'created_at': str(self.created_at),
            'updated_at': str(self.updated_at)
        }


class LogSystemBankAccount(utils.BaseModel):
    old_balance = models.CharField(max_length=20)
    new_balance = models.CharField(max_length=20)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    transaction = models.OneToOneField(Transaction, on_delete=models.CASCADE, blank=True, null=True)
    system_bank_account = models.ForeignKey(SystemBankAccount, on_delete=models.CASCADE)
    action = models.IntegerField(choices=utils.CHOICE_ACTION)


class LogSystemCreditAccount(utils.BaseModel):
    old_balance = models.CharField(max_length=20)
    new_balance = models.CharField(max_length=20)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    transaction = models.OneToOneField(Transaction, on_delete=models.CASCADE, blank=True, null=True)
    system_credit_account = models.ForeignKey(SystemCreditAccount, on_delete=models.CASCADE)
    action = models.IntegerField(choices=utils.CHOICE_ACTION)


class Setting(utils.BaseModel):
    key = models.CharField(max_length=255)
    value = models.TextField(blank=True, null=True)
    setting_type = models.IntegerField(default=utils.SETTING_TYPE_SYSTEM, choices=utils.CHOICE_SETTING_TYPE)

    def __str__(self):
        return self.key

    @classmethod
    def get_is_ignore_token_checked(cls, token):
        setting_token = Setting.objects.filter(key='ignore_token').first()
        return setting_token.value == token

    @property
    def context_data(self):
        return {
            [self.key]: self.value
        }
