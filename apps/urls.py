from django.urls import path

from apps.views import health_check, defaults

app_name = 'apps'

urlpatterns = [
    path('', defaults.index, name='index'),
    path('health/', health_check.health_status, name='health_check'),
    path('media/<filename>', defaults.media, name='media'),
]
