from django.http import HttpResponse, HttpResponseRedirect

from wallet.utils.s3_helper import S3Helper


def index(request):
    return HttpResponse('<html></html>')


def media(request, filename):
    s3_helper = S3Helper.get_instance()
    image_link = s3_helper.get_image_link(filename)
    return HttpResponseRedirect(image_link)
