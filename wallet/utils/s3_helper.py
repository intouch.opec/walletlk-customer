import hashlib
import os
import time
from io import BytesIO

import boto3
from PIL import Image
from django.conf import settings


class S3Helper:
    _instance = None
    client = None
    resource = None

    @staticmethod
    def get_instance():
        if S3Helper._instance is None:
            S3Helper()
        return S3Helper._instance

    def __init__(self):
        if S3Helper._instance is None:
            S3Helper._instance = self
            self.client = boto3.client('s3')
            self.resource = boto3.resource('s3')
        else:
            raise Exception("This class is a singleton!")

    def create_hash(self, url):
        hash = hashlib.sha1()
        hash.update(url)
        return hash.hexdigest()[:-10]

    def get_fullpath(self, image_path):
        # input is image_path
        # image_path = 7cd95b2f6bc7f2633b66315767b047.jpeg
        # return /047/67b/7cd95b2f6bc7f2633b66315767b047.jpeg
        filename = os.path.splitext(image_path)[0]
        subdir1 = filename[-3:]
        subdir2 = filename[-6:-3]
        image_key = os.path.join(subdir1, subdir2, image_path)
        return image_key

    def get_image_link(self, image_path, expire=86400):
        # return the image full link
        # image_path
        # /047/67b/7cd95b2f6bc7f2633b66315767b047.jpeg
        image_key = self.get_fullpath(image_path)
        image_url = self.signed_url(settings.S3_MEDIA_BUCKET_NAME, image_key, method='get_object', expire=expire)
        return image_url

    def signed_url(self, bucket, key, method='get_object', expire=86400):
        signed_url = self.client.generate_presigned_url(
            ClientMethod=method,
            Params={
                'Key': key,
                'Bucket': bucket
            },
            ExpiresIn=expire
        )
        return signed_url

    def upload_file_to_s3(self, image_io):
        out_im = BytesIO()
        im = Image.open(image_io)
        im.save(out_im, 'JPEG', quality=70)

        filename = '{0}_{1}'.format(image_io.name, int(time.time()))
        hash_filename = self.create_hash(filename.encode('utf-8'))
        filename = '{}.jpeg'.format(hash_filename)
        image_key = self.get_fullpath(filename)
        self.client.put_object(Bucket=settings.S3_MEDIA_BUCKET_NAME, Key=image_key, Body=out_im.getvalue(), ContentType='image/jpeg')
        return filename

    def delete_file_from_s3(self, filename):
        image_key = self.get_fullpath(filename)
        self.client.delete_object(Bucket=settings.S3_MEDIA_BUCKET_NAME, Key=image_key)
